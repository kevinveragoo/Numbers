import Cocoa

// Create with Type Annotation.
let score1: Int
score1 = 10

// Create by Inference.
let score = 10
let maxScore = 1_000_000

var workArea = maxScore - score

// Compound Assignment Operators
workArea += 5

// isMultipleOf
print(workArea.isMultiple(of: 2))

// Floating point number - Double
let number = 0.1 + 0.2
print(number)

// Type Safety
let a = 1
let b = 2.0
let c = Double(a) + b


// CGFloat and Double can be used interchangeably
let d: CGFloat = 2.2

